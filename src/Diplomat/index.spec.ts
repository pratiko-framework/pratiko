import { Diplomat } from '.';
import { Broker } from '../Broker';
import { Resolver } from '../Resolver';

jest.mock('node-nats-streaming');
jest.mock('koa');

test('Diplomat is defined', () => {
  expect(Diplomat).toBeDefined();
});

describe('new Diplomat', () => {
  const [noBroker, noResolver, ok]: any[] = [
    { resolvers: [new Resolver({ route: 'GET /foo', callback: () => {} })] },
    { broker: new Broker({ clientID: 'foo' }) },
    {
      broker: new Broker({ clientID: 'foo' }),
      resolvers: [new Resolver({ route: 'GET /foo', callback: () => {} })],
    },
  ];
  describe('Broker configuration', () => {
    it('is not required', () => {
      expect(() => new Diplomat(noBroker)).not.toThrow();
    });

    it('accepts a valid Broker object', () => {
      expect(() => new Diplomat(ok)).not.toThrow();
    });

    it('sets up a broker client', () => {
      const mockStan: any = {
        connect: jest.fn(() => {
          return {};
        }),
      };
      const app: Diplomat = new Diplomat(ok, mockStan);

      expect(app.broker).toBeDefined();
    });
  });

  describe('Resolver configuration', () => {
    it('is required', () => {
      expect(() => new Diplomat(noResolver)).toThrow();
    });

    it('is possible to access the list of Resolvers', () => {
      const resolvers: Resolver[] = [
        new Resolver({
          route: 'GET /foo',
          callback: (): void => {},
        }),
      ];

      const app: Diplomat = new Diplomat({
        resolvers,
      });

      expect(app.resolvers).toBe(resolvers);
    });
  });
});

describe('Diplomat.run()', () => {
  let appWithBroker: Diplomat;
  let appWithoutBroker: Diplomat;

  let mockStan: any;
  let mockConn: any;

  beforeEach(() => {
    mockConn = { on: jest.fn() };

    mockStan = {
      connect: jest.fn(() => {
        return mockConn;
      }),
    };

    const resolvers: Resolver[] = [
      new Resolver({ route: 'GET /foo', callback: (): void => {} }),
      new Resolver({
        name: 'baz',
        route: 'GET /baz',
        callback: (): void => {},
      }),
    ];

    appWithBroker = new Diplomat(
      {
        broker: new Broker({ clientID: 'foo' }),
        resolvers,
      },
      mockStan,
    );

    appWithoutBroker = new Diplomat(
      {
        resolvers,
      },
      mockStan,
    );
  });

  it('only tries to connect to NATS when a broker is provided', () => {
    appWithBroker.run();
    appWithoutBroker.run();

    expect(mockStan.connect).toHaveBeenCalledTimes(1);
  });
});
