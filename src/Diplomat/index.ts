import * as Stan from 'node-nats-streaming';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as logger from 'koa-logger';
import * as json from 'koa-json';

import { Component, ComponentConfig } from '../Component';

import { Broker } from '../Broker';
import { Resolver } from '../Resolver';

interface MetaRouter extends Router {
  [key: string]: any;
}

interface DiplomatConfig extends ComponentConfig {
  broker?: Broker;
  resolvers: Resolver[];
}

export class Diplomat extends Component {
  private _resolvers: Resolver[];
  private _app: Koa;
  private _router: MetaRouter;

  constructor(data: DiplomatConfig, stan: any = Stan) {
    super(data, stan);
    this._resolvers = data.resolvers;

    this._app = new Koa();
    this._app.use(logger());
    this._app.use(json());

    this._router = new Router();
  }

  public addContext(key: string, value: any): void {
    this._app.context[key] = value;
  }

  public get resolvers(): Resolver[] {
    return this._resolvers;
  }

  public run(port = 9090): void {
    this.resolvers.forEach((resolver: Resolver): void => {
      this._router[resolver.verb](resolver.uri, resolver.callback);
    });
    this._app.use(this._router.routes());
    this._app.use(this._router.allowedMethods());

    if (this.broker) {
      this.broker.on('connect', () => this._app.listen(port));
    } else {
      this._app.listen(port);
    }
  }

  protected validate(data: DiplomatConfig): void {
    if (data.broker && !(data.broker instanceof Broker)) {
      throw new Error();
    }

    const allResolvers: boolean = data.resolvers?.every(
      (el: Resolver): boolean => {
        return el instanceof Resolver;
      },
    );
    if (!data.resolvers || data.resolvers.length === 0 || !allResolvers) {
      throw new Error();
    }
  }
}
