import * as Stan from 'node-nats-streaming';

import { Broker } from '../Broker';

export interface ComponentConfig {
  broker?: Broker;
}

export class Component {
  private _broker: Stan.Stan | undefined;
  private stan: any;

  constructor(data: ComponentConfig, stan: any = Stan) {
    this.validate(data);

    if (data.broker) {
      this.stan = stan;
      this._broker = this.setupBroker(data.broker);
    } else {
      this._broker = undefined;
    }
  }

  public get broker(): Stan.Stan | undefined {
    return this._broker;
  }

  public run(): void {
    throw new Error();
  }

  protected validate(data: ComponentConfig): void {
    throw new Error();
  }

  private setupBroker(data: Broker): any {
    return this.stan.connect(data.clusterID, data.clientID, {
      url: data.url,
    });
  }
}
