import { Handler } from '.';

test('Handler is defined', () => {
  expect(Handler).toBeDefined();
});

test('Handler requires topic and callback', () => {
  expect(() => new Handler({})).toThrow();
  expect(() => new Handler({ topic: null })).toThrow();
  expect(() => new Handler({ callback: null })).toThrow();
  expect(() => new Handler({ topic: null, callback: null })).toThrow();
});

test('Handlers have optional names', () => {
  expect(
    () => new Handler({ topic: 'foo', callback: (): void => {}, name: 'baz' }),
  ).not.toThrow();
});

test('Handlers attributes are accessible for reading', () => {
  const topic = 'foo';
  const name = 'foo';
  const callback = (): void => {
    return;
  };

  const h: Handler = new Handler({
    topic,
    callback,
    name,
  });

  expect(h.topic).toBe(topic);
  expect(h.name).toBe(name);
  expect(h.callback).toBe(callback);
});
