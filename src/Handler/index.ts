interface HandlerConfig {
  topic: string;
  callback: Function;
  name?: string;
}

export class Handler {
  private _name: string | null;
  private _topic: string;
  private _callback: Function;

  constructor(data: HandlerConfig) {
    this.validate(data);

    this._name = data.name || null;
    this._topic = data.topic;
    this._callback = data.callback;
  }

  private validate(data: HandlerConfig): void {
    if (!data.topic || !data.callback) {
      throw new Error();
    }
  }

  public get name(): string | null {
    return this._name;
  }

  public get topic(): string {
    return this._topic;
  }

  public get callback(): Function {
    return this._callback;
  }
}
