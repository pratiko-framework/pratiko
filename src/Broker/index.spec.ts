import { Broker } from '.';

test('Broker is defined', () => {
  expect(Broker).toBeDefined();
});

test('Broker sets default value when clusterID and url are not provided', () => {
  const b: Broker = new Broker({ clientID: 'foo' });

  expect(b.clusterID).toBe('test-cluster');
  expect(b.url).toBe('nats://localhost:4222');
});

test('Broker accepts given values of clusterID and url', () => {
  const clusterID = 'module-broker';
  const url = 'nats://broker:4222';

  const b: Broker = new Broker({
    clientID: 'foo',
    clusterID,
    url,
  });

  expect(b.clusterID).toBe(clusterID);
  expect(b.url).toBe(url);
});
