interface BrokerConfig {
  clientID: string;
  clusterID?: string;
  url?: string;
}

export class Broker {
  public _clusterID: string;
  public _url: string;
  public _clientID: string;

  constructor(data: BrokerConfig) {
    this.validate(data);

    this._clientID = data.clientID;
    this._url = data.url || 'nats://localhost:4222';
    this._clusterID = data.clusterID || 'test-cluster';
  }

  public get clientID(): string {
    return this._clientID;
  }

  public get clusterID(): string {
    return this._clusterID;
  }

  public get url(): string {
    return this._url;
  }

  private validate(data: BrokerConfig) {
    if (!data.clientID) {
      throw new Error();
    }
  }
}
