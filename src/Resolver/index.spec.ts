import { Resolver } from '.';

test('Resolver is defined', () => {
  expect(Resolver).toBeDefined();
});

test('Resolver requires route and callback', () => {
  expect(() => new Resolver({})).toThrow();
  expect(() => new Resolver({ route: null })).toThrow();
  expect(() => new Resolver({ callback: null })).toThrow();
  expect(() => new Resolver({ route: null, callback: null })).toThrow();
});

test('Resolvers have optional names', () => {
  expect(
    () => new Resolver({ route: 'foo', callback: (): void => {}, name: 'baz' }),
  ).not.toThrow();
});

test('Resolvers attributes are accessible for reading', () => {
  const route = 'GET /foo';
  const name = 'foo';
  const callback = (): void => {
    return;
  };

  const r: Resolver = new Resolver({
    route,
    callback,
    name,
  });

  expect(r.verb).toBe('get');
  expect(r.uri).toBe('/foo');
  expect(r.name).toBe(name);
  expect(r.callback).toBe(callback);
});
