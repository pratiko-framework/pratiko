interface ResolverConfig {
  route: string;
  callback: Function;
  name?: string;
}

export class Resolver {
  private _name: string | null;
  private _verb: string;
  private _uri: string;
  private _callback: Function;

  constructor(data: ResolverConfig) {
    this.validate(data);

    this._name = data.name || null;
    [this._verb, this._uri] = data.route.toLowerCase().split(' ');
    this._callback = data.callback;
  }

  public get name(): string | null {
    return this._name;
  }

  public get verb(): string {
    return this._verb;
  }

  public get uri(): string {
    return this._uri;
  }

  public get callback(): Function {
    return this._callback;
  }

  private validate(data: ResolverConfig): void {
    if (!data.route || !data.callback) {
      throw new Error();
    }
  }
}
