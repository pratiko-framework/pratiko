import { Meerkat } from './index';
import { Broker } from '../Broker';
import { Handler } from '../Handler';

jest.mock('node-nats-streaming');

test('Meerkat is defined', () => {
  expect(Meerkat).toBeDefined();
});

describe('new Meerkat', () => {
  describe('Broker configuration', () => {
    it('is required', () => {
      expect(() => new Meerkat({})).toThrow();
      expect(() => new Meerkat({ broker: null })).toThrow();
      expect(
        () =>
          new Meerkat({
            handlers: [new Handler({ topic: 'foo', callback: (): void => {} })],
          }),
      ).toThrow();
    });

    it('accepts a valid Broker object', () => {
      expect(
        () =>
          new Meerkat({
            broker: new Broker({ clientID: 'foo' }),
            handlers: [new Handler({ topic: 'foo', callback: (): void => {} })],
          }),
      ).not.toThrow();
    });

    it('sets up a broker client', () => {
      const mockStan: any = {
        connect: jest.fn(() => {
          return {};
        }),
      };
      const app: Meerkat = new Meerkat(
        {
          broker: new Broker({ clientID: 'foo' }),
          handlers: [new Handler({ topic: 'foo', callback: (): void => {} })],
        },
        mockStan,
      );

      expect(app.broker).toBeDefined();
    });
  });

  describe('Handler configuration', () => {
    it('is required', () => {
      expect(() => new Meerkat({})).toThrow();
      expect(() => new Meerkat({ handlers: null })).toThrow();
      expect(() => new Meerkat({ handlers: [] })).toThrow();
      expect(
        () => new Meerkat({ broker: new Broker({ clientID: 'foo' }) }),
      ).toThrow();
    });

    it('is possible to access the list of Handlers', () => {
      const handlers: Handler[] = [
        new Handler({
          topic: 'foo',
          callback: (): void => {},
        }),
      ];

      const app: Meerkat = new Meerkat({
        broker: new Broker({ clientID: 'foo' }),
        handlers,
      });

      expect(app.handlers).toBe(handlers);
    });
  });
});

describe('context api', () => {
  let app: Meerkat;

  beforeEach(() => {
    app = new Meerkat({
      broker: new Broker({ clientID: 'foo' }),
      handlers: [new Handler({ topic: 'foo', callback: (): void => {} })],
    });
  });

  it('is possible to read from context', () => {
    expect(app.context).toBeDefined();
  });

  it('is possible to add to context', () => {
    app.context.foo = 'baz';

    expect(app.context.foo).toBe('baz');
  });
});

describe('Meerkat.run()', () => {
  let app: Meerkat;
  let mockStan: any;
  let mockConn: any;
  let mockSubs: any;

  beforeEach(() => {
    mockSubs = { on: jest.fn() };
    mockConn = {
      subscribe: jest.fn(() => mockSubs),
      on: jest.fn((_, callback) => callback()),
    };
    mockStan = {
      connect: jest.fn(() => mockConn),
    };
    app = new Meerkat(
      {
        broker: new Broker({ clientID: 'foo' }),
        handlers: [
          new Handler({ topic: 'foo', callback: (): void => {} }),
          new Handler({ name: 'baz', topic: 'baz', callback: (): void => {} }),
        ],
      },
      mockStan,
    );
  });

  it("subscribe to all handlers' topics", () => {
    app.run();

    expect(mockStan.connect).toHaveBeenCalled();
    expect(mockConn.subscribe).toHaveBeenCalledTimes(2);
    expect(mockSubs.on).toHaveBeenCalledTimes(2);
  });
});
