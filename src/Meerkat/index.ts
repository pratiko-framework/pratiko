import * as Stan from 'node-nats-streaming';

import { Component, ComponentConfig } from '../Component';

import { Broker } from '../Broker';
import { Handler } from '../Handler';

interface MeerkatConfig extends ComponentConfig {
  broker: Broker;
  handlers: Handler[];
}

export class Meerkat extends Component {
  private _handlers: Handler[];
  public readonly context: any = {};

  constructor(data: MeerkatConfig, stan: any = Stan) {
    super(data, stan);
    this._handlers = data.handlers;
  }

  public get handlers(): Handler[] {
    return this._handlers;
  }

  public run(): void {
    const broker = this.broker;
    const handlers = this.handlers;

    (this.broker as Stan.Stan).on('connect', () => {
      handlers.forEach((handler: Handler): void => {
        (broker as Stan.Stan)
          .subscribe(handler.topic, undefined)
          .on('message', (msg: Stan.Message) => {
            handler.callback(msg, this.context);
          });
      });
    });
  }

  protected validate(data: MeerkatConfig): void {
    if (!data.broker || !(data.broker instanceof Broker)) {
      throw new Error();
    }

    const allHandlers: boolean = data.handlers?.every(
      (el: Handler): boolean => {
        return el instanceof Handler;
      },
    );
    if (!data.handlers || data.handlers.length === 0 || !allHandlers) {
      throw new Error();
    }
  }
}
