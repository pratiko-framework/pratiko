module.exports = {
  title: 'Pratiko',
  description: 'A tangible event-driven microservices framework',
  themeConfig: {
    logo: '/logo.png',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Repo', link: 'https://gitlab.com/pratiko-framework/pratiko' },
    ],
  },
};
