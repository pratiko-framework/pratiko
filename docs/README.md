---
home: true
heroImage: /logo.png
actionText: Get Started >
actionLink: /guide/
features:
  - title: tangible
    details: have the feeling of drag-and-drop
  - title: event-driven
    details: forget ordering, decouple by annoucing
  - title: extensible
    details: make it yours by adding your stuff
---
