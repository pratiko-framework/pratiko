<img src="/pill-logo-text.png" alt="pratiko-framework" height="120" width="auto" />

Pratiko is a microservices framework derived from [Byron Framework](https://byron.netlify.com), from where it inherited the **event-driven microservices framework**. Pratiko is also **tangible** by adopting an interface inspired by Google's [Flutter](https://flutter.dev) framework for mobile development.

## Installation

Pratiko is hosted at NPM and can easily be install with your package manager:

```bash
# using YARN
yarn add @pratiko-framework/pratiko

# using NPM
npm install --save @pratiko-framework/pratiko
```

## Hello, outter World

"It's a pleasure to meet you!" you hear from the **Diplomat**, Pratiko's component responsible for handling with communication with external world. Let's dive into the code:

```ts
import { Diplomat, Resolver } from '@pratiko-framework/pratiko';

import { CreateUser } from 'path/to/CreateUser.ts';

export const app: Diplomat = new Diplomat({
  resolvers: [
    new Resolver({
      route: 'POST /users',
      callback: CreateUser,
    }),
  ],
});

app.run();
```

The **Diplomat** handles communication with external world by defining a set of **Resolvers**. Each resolver implements an exposed action thru Diplomat's API. That means, Diplomat exposes an _RESTful_ API and each endpoint is implemented with a Resolver.

Resolvers are a simple set of two or three informations: a **route** and a **callback**, which are both mandatory, and a **name**, which is opcional. The route is defined by a [HTTP verb](https://https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods) and a URI. The callback is the implementation for that endpoint and it follows this structure:

```ts
// CreateUser.ts
export const CreateUser = async (ctx: any): Promise<void> => {
  const { db }: any = ctx;
  const { email, password, confirmation }: any = ctx.args;

  // validate the data given, using rules and database, for instance

  if (isValid) {
    ctx.body = { user };
  } else {
    ctx.status = 400;
    ctx.body = { error: validationError };
  }
};
```

Notice some design decisions that might be unusual:

1. the successful case -- when `isValid === true` -- doesn't change the database directly, instead it publishes an event
2. the response is sent as soon as possible, without further due -- like updating the DB or receiving an ack back

## Hello, inner World

Since the external communication is well handled by the Diplomat, meet now the component responsible for watching every internal event and dispatching actions accordingly: the **Meerkat**. Let's dive into the code:

```ts
import { Meerkat, Broker, Handler } from '@pratiko-framework/pratiko';

import { CreateUser } from '/path/to/CreateUser.ts';

export const app: Meerkat = new Meerkat({
  broker: new Broker({
    clientID: 'module-meerkat',
  }),
  handlers: [
    new Handler({
      topic: 'new.user',
      callback: CreateUser,
    }),
  ],
});

app.run();
```

You might noticed that the Meerkat _requires_ a broker. The **Broker** is an object that wraps a connection to the NATS Streaming broker. That makes sense because Meerkat is responsible for the internal communication, i.e., it listens to events published all across the system and react accordingly.

Each event the Meerkat listens is declared in a **Handler** object, along-side with the reaction implementation, which is presented following:

```ts
export const CreateUser = async (msg: any, ctx: any): Promise<void> => {
  const { db }: any = ctx;
  const user = JSON.parse(msg.getData());

  await db.collection('users').insertOne(user);
};
```

The tipical behavior a Handler has is to extract data from messages -- representation of an event in the broker -- and update the database with that. But nothing stops you from doing other validations, transformations and even emitting events back to the broker as part of a choreography, a [Saga](https://microservices.io/patterns/data/saga.html) for instance.
