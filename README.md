# Pratiko

Pratiko - a Progressive Event-Driven Microservices Framework

## Installation

```bash
yarn add @pratiko-framework/pratiko
```

> You can also use NPM `npm install --save @pratiko-framework/pratiko`

## Usage

this module is part of the [Pratiko Framework](https://npmjs.com/org/pratiko-framework), working particularly well with the [Pratiko-cli](https://www.npmjs.com/package/@pratiko-framework/cli).

### Meerkat

Meerkat, as the name sugests, is a watcher for our module. It' supposed to keep an eye on every event that happens in the broker and act accordingly.

you can begin to develop a Meerkat component with:

```ts
import { Meerkat, Broker, Handler } from '@pratiko-framework/pratiko';

import { CreateUser } from 'path/to/CreateUser.ts';
import { CreateFoo } from 'path/to/CreateFoo.ts';

const app: Meerkat = new Meerkat({
  broker: new Broker({
    clientID: 'module-meerkat',
  }),
  handlers: [
    new Handler({ topic: 'new.user', callback: CreateUser }),
    new Handler({ name: 'foo creater', topic: 'new.foo', callback: CreateFoo }),
  ],
});

app.broker.on('connect', () => app.run());
```

### Diplomat

The Diplomat is the component responsible for dealing with the external policy. In other words, it's responsible for exposing an API, and dealing with HTTP requests.

you can begin to develop a Diplomat component with:

```ts
import { Diplomat, Broker, Resolver } from '@pratiko-framework/pratiko';

import { CreateUser } from 'path/to/CreateUser.ts';
import { CreateFoo } from 'path/to/CreateFoo.ts';

const app: Diplomat = new Diplomat({
  broker: new Broker({
    clientID: 'module-meerkat',
  }),
  handlers: [
    new Resolver({ route: 'POST /users', callback: CreateUser }),
    new Resolver({
      name: 'foo creater',
      route: 'POST /foos',
      callback: CreateFoo,
    }),
  ],
});

app.broker.on('connect', () => app.run());
```
